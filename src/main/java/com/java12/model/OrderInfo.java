package com.java12.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

import org.apache.tomcat.util.codec.binary.Base64;

@Entity
public class OrderInfo {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private Date createdDate;
	private Date expectedDate;
	
	@Lob
	private byte[] image;
	private String nameProduct;
	private String supplier;
	private int price;
	private int quantity;
	private int feeDelivery;
	private String status;
	private int discount;
	private String address;
	private Long userId;
	private int total;
	
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getExpectedDate() {
		return expectedDate;
	}

	public void setExpectedDate(Date expectedDate) {
		this.expectedDate = expectedDate;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getNameProduct() {
		return nameProduct;
	}

	public void setNameProduct(String nameProduct) {
		this.nameProduct = nameProduct;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getFeeDelivery() {
		return feeDelivery;
	}

	public void setFeeDelivery(int feeDelivery) {
		this.feeDelivery = feeDelivery;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public OrderInfo(Date createdDate, Date expectedDate, byte[] image, String nameProduct, String supplier,
			int price, int quantity, int feeDelivery, String status, int discount, String address, Long userId,
			int total) {
		
		this.createdDate = createdDate;
		this.expectedDate = expectedDate;
		this.image = image;
		this.nameProduct = nameProduct;
		this.supplier = supplier;
		this.price = price;
		this.quantity = quantity;
		this.feeDelivery = feeDelivery;
		this.status = status;
		this.discount = discount;
		this.address = address;
		this.userId = userId;
		this.total = total;
	}

	public OrderInfo(Date createdDate, Date expectedDate, byte[] image, String nameProduct, String supplier,
			int price, int quantity, int feeDelivery, String status, int discount, String address, Long userId
			) {
		
		this.createdDate = createdDate;
		this.expectedDate = expectedDate;
		this.image = image;
		this.nameProduct = nameProduct;
		this.supplier = supplier;
		this.price = price;
		this.quantity = quantity;
		this.feeDelivery = feeDelivery;
		this.status = status;
		this.discount = discount;
		this.address = address;
		this.userId = userId;
		this.total = this.price*this.quantity+this.feeDelivery-this.discount;
	}
	
	public OrderInfo(Date createdDate, Date expectedDate, String nameProduct, String supplier,
			int price, int quantity, int feeDelivery, String status, int discount, String address, Long userId
			) {
		
		this.createdDate = createdDate;
		this.expectedDate = expectedDate;
		this.nameProduct = nameProduct;
		this.supplier = supplier;
		this.price = price;
		this.quantity = quantity;
		this.feeDelivery = feeDelivery;
		this.status = status;
		this.discount = discount;
		this.address = address;
		this.userId = userId;
		this.total = this.price*this.quantity+this.feeDelivery-this.discount;
	}

	public OrderInfo() {
		
	}
	
	public String generateBase64Image()
	{
	    return Base64.encodeBase64String(this.getImage());
	}
}
