package com.java12.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
public class Video {
	@Id
	@GeneratedValue 
	private Integer id;
	private String title;
	private String thumbnail;
	private String srcVideo;
	private Date created;
	private Long view;
	
	@Lob
	private String description;

	public Video(String title, String thumbnail, String srcVideo, Date created, Long view, String description) {
		super();

		this.title = title;
		this.thumbnail = thumbnail;
		this.srcVideo = srcVideo;
		this.created = created;
		this.view = view;
		this.description = description;
	}
	
	public Video() {
		super();
	}
	
	public Video(Integer id, String title, String thumbnail, String srcVideo, Date created, Long view,
			String description) {
		super();

		this.id = id;
		this.title = title;
		this.thumbnail = thumbnail;
		this.srcVideo = srcVideo;
		this.created = created;
		this.view = view;
		this.description = description;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	public String getSrcVideo() {
		return srcVideo;
	}
	public void setSrcVideo(String srcVideo) {
		this.srcVideo = srcVideo;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Long getView() {
		return view;
	}
	public void setView(Long view) {
		this.view = view;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
