package com.java12.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java12.model.Video;
import com.java12.repositories.VideoRepository;

@Service
public class VideoService {
	
	@Autowired
	VideoRepository repo;
	
	public List<Video> getAllVideos(){		
		return repo.findAll();
	}
	
	public Video getVideoById(int id) {
		return repo.findById(id);
	}
	
	public Video addVideo(Video video) {
		repo.save(video);
		return video;
	}
	
	public boolean editVideo(Video video) {
		Video tempVideo = repo.findById(video.getId()).get();
		if(tempVideo != null) {
			tempVideo.setTitle(video.getTitle());
			tempVideo.setThumbnail(video.getThumbnail());
			tempVideo.setSrcVideo(video.getSrcVideo());
			tempVideo.setCreated(video.getCreated());
			tempVideo.setDescription(video.getDescription());
			tempVideo.setView(video.getView());
			repo.save(tempVideo);
			return true;
		}
		return false;
	}
	
	public void deleteVideo(int id) {
		repo.deleteById(id);
	}

}
