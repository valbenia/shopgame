package com.java12.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java12.model.OrderInfo;
import com.java12.repositories.OrderInfoRepository;

@Service
public class OrderInfoService {
	@Autowired
	OrderInfoRepository orderRepo;
	
	public List<OrderInfo> getUserOrders(Long userId) {
		List<OrderInfo> listOrders = orderRepo.findAll();
		List<OrderInfo> result = new ArrayList();
		for(OrderInfo item:listOrders) {
			if(item.getUserId().equals((Long)userId)) {
				result.add(item);
			}
		}
		return result;
	}
}
