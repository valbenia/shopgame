package com.java12.service;

import com.java12.model.Album;
import com.java12.repositories.AlbumRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class AlbumService {
    @Autowired
    AlbumRepository albumRepo;
    
    public List<Album> listAlbums(){
        return albumRepo.findAll();
    }
    
    public void add(Album album) {
    	albumRepo.saveAndFlush(album);
    }
    
    public Album getAlbum(Long id) {
    	return albumRepo.findById(id).get();
    }
    
    public void deleteAlbum(Long id) {
    	 albumRepo.deleteById(id);
    	 }
    
    public void save(Album album) {
    	albumRepo.save(album);
     }
}
