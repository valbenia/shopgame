package com.java12.service;

import com.java12.model.User;
import com.java12.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    UserRepository userRepo;

    public Optional<User> loadUserByEmail(String email){
        return userRepo.findByEmail(email);
    }

    public Optional<User> loadUserByPhoneNumber(String phoneNumber){
        return userRepo.findByPhoneNumber(phoneNumber);
    }

    public Optional<User> loadUserById(long id){
        return userRepo.findById(id);
    }

    public void saveUser(User user){
        userRepo.saveAndFlush(user);
    }
    
    // Dai Tran
    public List<User> listAll(){
    	return userRepo.findAll();
    }
    public User getUser(Long id) {
    	return userRepo.findById(id).get();
    }
    public void save(User user) {
    	userRepo.save(user);
    }
    public void deleteUser(Long id) {
    	userRepo.deleteById(id);
    }
    public List<String> listAllRole() {
    	return userRepo.listAllRole();
    }
}
