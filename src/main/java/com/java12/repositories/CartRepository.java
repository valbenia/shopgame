package com.java12.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.java12.model.Cart;
import com.java12.model.News;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long>{
	
}