/**
 * @author LiemNT
 */
package com.java12.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.java12.model.Images;




@Repository
public interface ImageRepository extends JpaRepository<Images, Long> {
	
	List<Images> findAll();
	
    @Query(value = "SELECT * FROM images i where i.id_album = ?1", nativeQuery = true)
    List<Images> findAllByAlbum(Long id_album);

}
