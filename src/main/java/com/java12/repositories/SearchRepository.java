package com.java12.repositories;

import com.java12.model.Product;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SearchRepository extends PagingAndSortingRepository<Product, String> {

    @Query(value = "SELECT * FROM product p WHERE name LIKE %?1%",nativeQuery = true)
    Page<Product> findByNameContaining(String keywork, Pageable pageable);
}
