package com.java12.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.java12.model.OrderInfo;

public interface OrderInfoRepository extends JpaRepository<OrderInfo, Long> {

}
