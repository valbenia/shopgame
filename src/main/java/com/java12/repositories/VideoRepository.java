package com.java12.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.java12.model.Video;

@Repository
public interface VideoRepository extends JpaRepository<Video, Integer> {
	
	Video findById(int id);
	
}
