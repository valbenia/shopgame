package com.java12.repositories;

import com.java12.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findById(long id);
    Optional<User> findByEmail(String email);
    Optional<User> findByPhoneNumber(String phoneNumber);
    User saveAndFlush(User user);
    
	/* Quang Dai */
    @Query(value = "SELECT DISTINCT role FROM user", nativeQuery = true)
	List<String> listAllRole();
}
