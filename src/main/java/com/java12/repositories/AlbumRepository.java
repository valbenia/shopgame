package com.java12.repositories;
/**
 * @author LiemNT
 */
import com.java12.model.Album;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AlbumRepository extends JpaRepository<Album, Long> {

	
}
