package com.java12.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.java12.model.Contact;
import com.java12.model.User;
import com.java12.service.ContactService;
import com.java12.service.UserService;

@Controller
@RequestMapping("/users")

public class UsersController {
	
	@Autowired
	UserService userService;
	@Autowired
	 ContactService contactService;
	
	@RequestMapping
	public String showUsers(Model model, HttpSession session) {
		if (session.getAttribute("userId") == null || !session.getAttribute("userRole").equals("ADMIN") ) {
			return "401";
		}
		List<User> listUsers = userService.listAll();
		model.addAttribute("users", listUsers);
		return "manage_user";
	}
	
	@GetMapping(value = "/{id}") 
	public String showEditUserForm(@PathVariable Long id, Model model, HttpSession session) {
		if (session.getAttribute("userId") == null || !session.getAttribute("userRole").equals("ADMIN") ) {
			return "401";
		}
		model.addAttribute("user", userService.getUser(id)); 
		List<String> listRole = userService.listAllRole();
		model.addAttribute("listRole", listRole);
		return "edit_user";
		
	}
	
	@PostMapping(value = "/save")
	public String editUser(@ModelAttribute("user") User user, HttpSession session) {
		if (session.getAttribute("userId") == null || !session.getAttribute("userRole").equals("ADMIN") ) {
			return "401";
		}
		userService.save(user);
		if (user!= null && user.getRole().equals("ADMIN")) {
			return "redirect:/users";
		}
		return "redirect:/";
	}
	
	@RequestMapping(value = "/delete/{id}")
	public String deleteUser(@PathVariable Long id, Model model, HttpSession session) {
		if (session.getAttribute("userId") == null || !session.getAttribute("userRole").equals("ADMIN") ) {
			return "401";
		}
		userService.deleteUser(id);
		return "redirect:/users";
	}
}
