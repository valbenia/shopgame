package com.java12.controller;

import java.awt.Dialog.ModalExclusionType;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.java12.model.Video;
import com.java12.service.VideoService;

@Controller
public class VideoController {
	
	@Autowired
	VideoService service;
	
	@RequestMapping("/video")
	public String getAllVideo(Model model) {
		List<Video> lstVideos = new ArrayList<Video>();
		lstVideos = service.getAllVideos();
		Video firstVideo = lstVideos.get(0);
		List<Video> videos = lstVideos.subList(1, lstVideos.size());
		model.addAttribute("firstVideo",firstVideo);
		model.addAttribute("lstVideos", videos);
		return "video";
	}
	
	@RequestMapping("/video/{id}")
	public String getVideoById(@PathVariable("id") int id, Model model){
		Video video = new Video();
		video = service.getVideoById(id);
		model.addAttribute("video", video);		
		List<Video> lstVideos = new ArrayList<Video>();
		lstVideos = service.getAllVideos();
		model.addAttribute("lstVideos", lstVideos);	
		return "showVideo";
	}
	
	//55555555
	
	@RequestMapping("/video/new")
	public String showNewVideoPage(Model model) {
		Video video = new Video();
		model.addAttribute("video", video);
		return "videoAddNew";
	}
	
	@RequestMapping(value = "/video/save", method = RequestMethod.POST)
	public String saveVideo(@ModelAttribute("video") Video video) {
		long millis=System.currentTimeMillis();
		java.sql.Date date=new java.sql.Date(millis);
		video.setCreated(date);
		video.setView((long)0);
		service.addVideo(video);
		return "redirect:/video";
	}
	
	@RequestMapping(value = "/video/update", method = RequestMethod.POST)
	public String updateVideo(@ModelAttribute("video") Video video) {
		service.addVideo(video);
		return "redirect:/video/"+video.getId();
	}
	
	@RequestMapping("/video/edit/{id}")
	public ModelAndView showEditVideoPage(@PathVariable(name = "id") int id) {
		ModelAndView mav = new ModelAndView("videoUpdate");
		Video video = service.getVideoById(id);
		mav.addObject("video", video);
		return mav;
	}
	
	@RequestMapping("/video/delete/{id}")
	public String deleteVideo(@PathVariable(name = "id") int id) {
		service.deleteVideo(id);
		return "redirect:/video";		
	}
	
	@RequestMapping("/video/guide")
	public String guide() {

		return "guide-add-video";		
	}
	
}
