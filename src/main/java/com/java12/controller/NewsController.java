package com.java12.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.java12.model.News;
import com.java12.service.ContactService;
import com.java12.service.NewsService;

@Controller
public class NewsController {
	
	@Autowired
	NewsService newsService;
	@Autowired
	 ContactService contactService;
	@GetMapping("/news")
	public String getAllNews(Model model) {
		List<News> lstNews = new ArrayList<News>();
		lstNews =newsService.getAllNews();
		
		model.addAttribute("listNews",lstNews);
		model.addAttribute("test","hello");
		return "news";
	}
	
	@RequestMapping("/news/{id}")
	public String detailNews(Model model, @PathVariable("id") int id) {
		News news = newsService.findNewsById(id);
		model.addAttribute("news",news);
		List<News> hotNews = newsService.findHotNews();
		List<News> viewMoreNews= newsService.findViewMoreNews();
	
		model.addAttribute("hot",hotNews);
		model.addAttribute("viewMore",viewMoreNews);
		return "news-detail";
	}
	
	/* Quang Dai - Add News */
	@RequestMapping("/news/add")
	public String addNews(Model model, HttpSession session) {
		if (session.getAttribute("userId") == null || !session.getAttribute("userRole").equals("ADMIN") ) {
			return "401";
		}
		News news = new News();
		model.addAttribute("standardDate", new Date());
		model.addAttribute("news", news);
		model.addAttribute("listCategory", newsService.listAllCategory());
		return "add_news";
	}
	
	@PostMapping(value = "/news/save")
	public String editUser(@ModelAttribute("news") News news, @RequestParam("imageNew") MultipartFile multipartFile) throws IOException {
		News newsImg = newsService.getNews(news.getId());
		if(multipartFile.isEmpty()==true) { 
			news.setImage(newsImg.getImage());
		}
		else {
			String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
			news.setImage(multipartFile.getBytes());
		}
		newsService.addNew(news);
		return "redirect:/news";
	}
	
	@RequestMapping("/news/edit/{id}")
	public String editNews(Model model, @PathVariable("id") int id, HttpSession session) {
		if (session.getAttribute("userId") == null || !session.getAttribute("userRole").equals("ADMIN") ) {
			return "401";
		}
		News news = newsService.findNewsById(id);
		model.addAttribute("news",news);
		return "edit_news";
	}
	
	@RequestMapping("/news/category/{category}")
	public String getNewsByCategory(Model model,@PathVariable("category") String category) {
		String tinTuc="Tin tức y khoa - thiết bị y tế",
		 thongTin="Thông tin đào tạo – huấn luyện",
		 coHoi="Cơ hội kinh doanh – Giao lưu",
		 hoiDap="Hỏi đáp",
		 tuyenDung="Tuyển dụng- tìm việc",
		 lienHe="Liên hệ - Góp ý";
		List<News> result = new ArrayList<News>();
		switch (category) {
		case "tin-tuc-y-khoa-thiet-bi-y-te":{
			result = newsService.getNewsByCategory(tinTuc);
			model.addAttribute("category",tinTuc);
			break;
		}
			
			
		case "thong-tin-dao-tao-huan-luyen":{
			result = newsService.getNewsByCategory(thongTin);
			model.addAttribute("category",thongTin);
			break;
		}
			
		case "co-hoi-kinh-doanh-giao-luu":{
			result = newsService.getNewsByCategory(coHoi);
			model.addAttribute("category",coHoi);
			break;
		}

			
		case "hoi-dap":{
			result = newsService.getNewsByCategory(hoiDap);
			model.addAttribute("category",hoiDap);
			break;
		}
	
		case "tuyen-dung-tim-viec":{
			result = newsService.getNewsByCategory(tuyenDung);
			model.addAttribute("category",tuyenDung);
			break;
		}
		case "lien-he-gop-y":{
			result = newsService.getNewsByCategory(lienHe);
			model.addAttribute("category",lienHe);
			break;
		}

		default:
			break;
		}
		model.addAttribute("listNews",result);
		 
		return "news-category";
	}
	@RequestMapping("news/delete/{id}")
	public String deleteNews(Model model, @PathVariable("id") int id, HttpSession session) {
		if (session.getAttribute("userId") == null || !session.getAttribute("userRole").equals("ADMIN") ) {
			return "401";
		}
		newsService.deleteNews(id);
		return "redirect:/news";
	}
}
