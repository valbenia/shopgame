package com.java12.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.java12.model.Contact;
import com.java12.model.Item;
import com.java12.model.Product;
import com.java12.model.Album;
import com.java12.model.Cart;
import com.java12.service.CartService;
import com.java12.service.ContactService;
import com.java12.service.ProductService;
import com.java12.service.UserService;

@Controller
public class CartController {
	@Autowired
	CartService cartService;
	@Autowired 
	ProductService productService;
//	@GetMapping("/productDetail")
	@GetMapping("/addCart")
	public String addCart(@RequestParam(name = "orderQuantity", required = false, defaultValue = "0") int productQua,
			@RequestParam(name = "orderProductId", required = false, defaultValue = "0") int productId,
			Model model,HttpSession session) throws IOException {
		
		if(productQua != 0 && productId != 0) {
			List<Cart> listItems = cartService.listItems();
			int check =0;
			for(Cart cart: listItems) {
				if(cart.getProductId() == productId && (boolean)( Long.valueOf(cart.getUserId()).equals((Long)session.getAttribute("userId")))) {
					check=1;
					break;
				}
			}
			if(check <1) {
				Cart cart = new Cart(productQua, 0, productId,(Long)session.getAttribute("userId"));
				cartService.add(cart);
			}
			
		}
//		Cart cart = new Cart(productQua, 0, productId);
		
//		Long userId = (Long) session.getAttribute("userId");
//		System.out.print("\n\n\n" + userId);
//		List<Cart> listItems = cartService.getCartByUserId((long) userId) ;
		List<Cart> listItems = cartService.listItems();
		List<Item> item = new ArrayList(); //item ~ product in cart
		
		for (Cart cart: listItems) {
//			System.out.println("\n\n\n" + cart.getId());
			item.add(new Item((Long)cart.getId(), cartService.findItem(cart.getProductId()),cart.getQuantityP()));
		}
//		for(Item carrrt : item) {
//			System.out.println("\n\n\n id:" + carrrt.getId());
//		}
		model.addAttribute("listProducts", item);
		return "addCart";
	}
	
	@RequestMapping("/addCart/delete/{id}")
	public String deleteItem(@PathVariable Long id, Model model) {
		cartService.DeleteCart(id);
		return "redirect:/addCart";
	}
	@RequestMapping("/payment")
	public String pay(@RequestParam(name = "orderQuantity", required = false, defaultValue = "0") int productQua,
			@RequestParam(name = "orderProductId", required = false, defaultValue = "0") int productId,
			Model model,HttpSession session) throws IOException {
		
		if(productQua != 0 && productId != 0) {
			List<Cart> listItems = cartService.listItems();
			int check =0;
			for(Cart cart: listItems) {
				if(cart.getProductId() == productId && (boolean)( Long.valueOf(cart.getUserId()).equals((Long)session.getAttribute("userId")))) {
					check=1;
					break;
				}
			}
			if(check <1) {
				Cart cart = new Cart(productQua, 0, productId,(Long)session.getAttribute("userId"));
				cartService.add(cart);
			}
			
		}
//		Cart cart = new Cart(productQua, 0, productId);
		
//		Long userId = (Long) session.getAttribute("userId");
//		System.out.print("\n\n\n" + userId);
//		List<Cart> listItems = cartService.getCartByUserId((long) userId) ;
		List<Cart> listItems = cartService.listItems();
		List<Item> item = new ArrayList(); //item ~ product in cart
		int sum = 0;
		for (Cart cart: listItems) {
//			System.out.println("\n\n\n" + cart.getId());
			Item newItem = new Item((Long)cart.getId(), cartService.findItem(cart.getProductId()),cart.getQuantityP());
			item.add(newItem);
			sum = sum + Integer.parseInt(newItem.getProduct().getPrice())*newItem.getQuantityInCart();
		}
//		for(Item carrrt : item) {
//			System.out.println("\n\n\n id:" + carrrt.getId());
//		}
		model.addAttribute("listProducts", item);
		model.addAttribute("sum", sum);
		return "payment";
	}
	
	// /successPayment 
	
	@GetMapping("/addCart/successPayment")
	public String SuccessPayment(Model model, HttpSession session) {
		List<Cart> listItems = cartService.listItems();
		Long userId = (Long) session.getAttribute("userId");
		for (Cart cart: listItems) {
			if (cart.getUserId().equals(userId)) {
				Product product = productService.findProductById(cart.getProductId());
				product.setQuantity(product.getQuantity() - cart.getQuantityP());
				productService.addProduct(product);
				cartService.DeleteCart(cart.getId());
			}
		}
		// cartService.DeleteCart(id);
		return "redirect:/addCart";
	}
}
