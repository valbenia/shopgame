package com.java12.mds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.java12.config.VideoStorageProperties;

@EnableConfigurationProperties({VideoStorageProperties.class})
@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
@EnableJpaRepositories("com.java12.repositories")
@EntityScan("com.java12.model")
@ComponentScan(basePackages = "com.java12")
public class MdsApplication {
	public static void main(String[] args) {
		SpringApplication.run(MdsApplication.class, args);
	}

}
